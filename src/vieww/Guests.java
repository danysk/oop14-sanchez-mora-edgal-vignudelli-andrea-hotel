/*
 * Questa gui permette di inserire gli ospiti a una stanza, 
 * tenendo conto dei dati registrati in fase di prenotazione
 */
package vieww;

import java.awt.Dimension;

import java.time.LocalDate;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.Booking;
import model.Guest;
import model.Hotel;
import controller.Controller;

// TODO: Auto-generated Javadoc
/**
 * The Class Guests.
 */
public class Guests extends JFrame {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5732151418892442032L;

	/** The name label. */
	private final JLabel nameLabel = new JLabel("Nome dell'ospite: ");
	
	/** The name text. */
	private final JTextField nameText = new JTextField();
	
	/** The surname label. */
	private final JLabel surnameLabel = new JLabel("Cognome dell'ospite: ");
	
	/** The surname text. */
	private final JTextField surnameText = new JTextField();
	
	/** The cf label. */
	private final JLabel cfLabel = new JLabel("CF dell'ospite: ");
	
	/** The cf text. */
	private final JTextField cfText = new JTextField();
	
	/** The birth label. */
	private final JLabel birthLabel = new JLabel("Data di nascita dell'ospite: ");
	
	/** The birth text. */
	private final JTextField birthText = new JTextField();
	
	/** The add. */
	private final JButton add = new JButton("Aggiungi");

	/**
	 * Instantiates a new guests.
	 *
	 * @param b as the booking to add the guest
	 */
	Guests(final Booking b) {
		Dimension d = new Dimension(100, 200);
		this.setTitle("Inserimento cliente/ospite");
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));
		this.add(nameLabel);
		this.add(nameText);
		nameText.setMaximumSize(d);
		this.add(surnameLabel);
		this.add(surnameText);
		surnameText.setMaximumSize(d);
		this.add(birthLabel);
		this.add(birthText);
		birthText.setMaximumSize(d);
		this.add(cfLabel);
		this.add(cfText);
		cfText.setMaximumSize(d);
		this.add(add);
		add.addActionListener(e -> {
			if (!(Controller.checkIfContainsNumber(nameText.getText()))
					&& !(Controller.checkIfContainsNumber(surnameText.getText()))) {
				if (cfText.getText().length() == 16) {
					if (Controller.formatLocalDate(birthText.getText()) != null
							&& LocalDate.now().isAfter(Controller.formatLocalDate(birthText.getText()))) {
						Guest g = new Guest(nameText.getText(), surnameText.getText(), cfText.getText(),
								Controller.formatLocalDate(birthText.getText()));
						if (g.getAge() <= Hotel.getInstance().getCatalog().getBabyAge()) {
							int count = 0;
							for (Guest g2 : b.getGuestsList()) {
								if (g2.getAge() <= Hotel.getInstance().getCatalog().getBabyAge()) {
									count++;
								}
							}
							if (count < b.getDeclaredBabies()) {
								Hotel.getInstance().addGuest(new Guest(nameText.getText(), surnameText.getText(),
										cfText.getText(), Controller.formatLocalDate(birthText.getText())));
								b.getGuestsList().add(new Guest(nameText.getText(), surnameText.getText(),
										cfText.getText(), Controller.formatLocalDate(birthText.getText())));
							} else {
								// OPTIONPANE
							}
						} else if (g.getAge() <= Hotel.getInstance().getCatalog().getChildAge()) {
							int count = 0;
							for (Guest g2 : b.getGuestsList()) {
								if (g2.getAge() <= Hotel.getInstance().getCatalog().getBabyAge()) {
									count++;
								}
							}
							if (count < b.getDeclaredChildren()) {
								Hotel.getInstance().addGuest(new Guest(nameText.getText(), surnameText.getText(),
										cfText.getText(), Controller.formatLocalDate(birthText.getText())));
								b.getGuestsList().add(new Guest(nameText.getText(), surnameText.getText(),
										cfText.getText(), Controller.formatLocalDate(birthText.getText())));
							} else {
								// optionpane
							}
						} else {
							int count = 0;
							for (Guest g2 : b.getGuestsList()) {
								if (g2.getAge() <= Hotel.getInstance().getCatalog().getBabyAge()) {
									count++;
								}
							}
							if (count < b.getDeclaredAdults()) {
								Hotel.getInstance().addGuest(new Guest(nameText.getText(), surnameText.getText(),
										cfText.getText(), Controller.formatLocalDate(birthText.getText())));
								b.getGuestsList().add(new Guest(nameText.getText(), surnameText.getText(),
										cfText.getText(), Controller.formatLocalDate(birthText.getText())));
							}
						}
					} else {
						JOptionPane.showMessageDialog(this, "Data non valida");
					}
				} else {
					JOptionPane.showMessageDialog(this, "Campo CF non valido");
				}
			} else {
				JOptionPane.showMessageDialog(this, "I campi nome e cognome non devono contenere numeri");
			}

		});

		this.pack();
		this.setVisible(true);

	}

}