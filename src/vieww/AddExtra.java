/**
 * La gui permette di inserire le voci di extra, con il relativo costo, e di memorizzarlo all'interno
 * del sistema. La visualizzazione degli extra avviene tramite JList
 *
 * @see         AddExtra
 */
package vieww;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import controller.Controller;
import model.Extra;
import model.Hotel;

// TODO: Auto-generated Javadoc
/**
 * The Class AddExtra.
 */
public class AddExtra extends JFrame {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The add extra button. */
	private final JButton addExtra = new JButton("Inserisci nuova voce di extra");
	
	/** The delete extra button. */
	private final JButton deleteExtra = new JButton("Elimina voce di extra");
	
	/** The model for JList. */
	private final DefaultListModel<Extra> model = new DefaultListModel<>();
	
	/** The Jlist of extra. */
	private  JList<Extra> list ;
	
	/** The buttons panel. */
	private final JPanel buttonsPanel = new JPanel();
	
	/** The close button. */
	private final JButton close = new JButton("Indietro");

	/**
	 * Instantiates a new adds the extra.
	 */
	AddExtra() {
		this.setTitle("Inserimento voci extra");
		this.setSize(600, 250);
		this.setLayout(new BorderLayout());
		if (Hotel.getInstance().getExtraList() != null && Hotel.getInstance().getExtraList().size() != 0) {
				for (Extra e : Hotel.getInstance().getExtraList()) {
					model.addElement(e);
				}
			}
		list = new JList<>(model);

		final JScrollPane pane = new JScrollPane(list);

		this.add(pane, BorderLayout.WEST);
		buttonsPanel.add(addExtra);
		buttonsPanel.add(deleteExtra);
		buttonsPanel.add(close);
		deleteExtra.addActionListener(e -> {
			try {
				if (list.getSelectedIndex() >= 0) {
					model.remove(list.getSelectedIndex());
					Hotel.getInstance().removeExtra(model.get(list.getSelectedIndex()));
				}
			} catch (ArrayIndexOutOfBoundsException ef) {

			}
		});
		addExtra.addActionListener(e -> {
			launchDialog();
		});
		close.addActionListener(e ->{
			this.dispose();
		});
		this.add(buttonsPanel);
		this.setVisible(true);
		
	}

	/**
	 * Launch dialog. 
	 */
	
	private void launchDialog() {
		final JButton add = new JButton("Aggiungi");
		final JLabel voiceLabel = new JLabel("Inserici il servizio extra: ");
		final JLabel amountLabel = new JLabel("Inserici importo: ");
		final JTextField voice = new JTextField(10);
		final JTextField amount = new JTextField(10);
		final JDialog dialog = new JDialog(this, true);
		add.addActionListener(e -> {
			String[] information = new String[2];
			information[0] = voice.getText();
			information[1] = amount.getText();
			if (Controller.checkIfIsNumber(amount.getText()) && amount.getText().length() > 0
					&& voice.getText().length() > 0) {
				final Extra extra = new Extra(voice.getText(), Double.parseDouble(amount.getText()));
				Hotel.getInstance().addExtra(extra);
				model.addElement(extra);
				dialog.dispose();
				MainFrame.getInstance().updateButtons();
			} else {
				JOptionPane.showMessageDialog(null, "Errore", "Campi non validi", JOptionPane.ERROR_MESSAGE);
			}
		});
		final JPanel panel = new JPanel(new FlowLayout());

		dialog.getContentPane().add(panel);
		panel.add(voiceLabel);
		panel.add(voice);
		panel.add(amountLabel);
		panel.add(amount);
		panel.add(add);
		dialog.pack();
		dialog.setLocation(525, 200);
		dialog.setVisible(true);
	}
}