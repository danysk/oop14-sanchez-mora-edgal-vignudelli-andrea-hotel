/*Quest'interfaccia permette la ricerca, inserendo un nome e un cognome, la visualizzazione di tutte le prenotazioni
 * associate e permette la cancellazione di esse.
 * 
 */
package vieww;

import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import controller.Controller;
import model.Booking;
import model.Customer;
import model.Guest;
import model.Hotel;

// TODO: Auto-generated Javadoc
/**
 * The Class SearchBooking.
 */
public class SearchBooking extends JFrame {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7921756532696608117L;

	/** The name label. */
	private final JLabel nameLabel = new JLabel("Nome dell'ospite: ");
	
	/** The name text field. */
	private final JTextField nameText = new JTextField(20);
	
	/** The surname label. */
	private final JLabel surnameLabel = new JLabel("Cognome dell'ospite: ");
	
	/** The surname text field. */
	private final JTextField surnameText = new JTextField(20);
	
	/** The button search. */
	private final JButton search = new JButton("Ricerca");
	
	/** The model for JList*/
	private DefaultListModel<Booking> model = new DefaultListModel<>();
	
	/** The Jlist. */
	private JList<Booking> list = new JList<>(model);
	
	/** The delete selected booking button. */
	private final JButton deleteSelectedBooking = new JButton("Rimuovi");
	
	/** The search panel. */
	private final JPanel searchPanel = new JPanel();
	
	/** The result panel. */
	private final JPanel resultPanel = new JPanel();
	
	/** The pane contains list. */
	private JScrollPane pane = new JScrollPane(list);
	
	/**
	 * Instantiates a new search booking.
	 *
	 * @param rv the roonView
	 */
	SearchBooking(final RoomView rv) {
		this.setTitle("Ricerca prenotazione");
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));
		this.add(searchPanel);
		this.add(resultPanel);
		searchPanel.add(nameLabel);
		searchPanel.add(nameText);
		searchPanel.add(surnameLabel);
		searchPanel.add(surnameText);
		searchPanel.add(search);
		resultPanel.add(pane);
		resultPanel.add(deleteSelectedBooking);
		search.addActionListener(e -> {
			if (!Controller.checkIfContainsNumber(nameText.getText())
					&& !Controller.checkIfContainsNumber(surnameText.getText())) {
				List<Guest> list = Hotel.getInstance().findSomeone(nameText.getText(), surnameText.getText());
				if (list.size() > 0) {
					for (Guest g : list) {
						if (g instanceof Customer) {
							if (!model.contains(((Customer) g).getBooking()))
								model.addElement(((Customer) g).getBooking());
						}
					}
					this.revalidate();
					this.repaint();
				}
			}

		});
		deleteSelectedBooking.addActionListener(c -> {
			if (this.list.getSelectedIndex() >= 0) {
				Hotel.getInstance().removeBooking(this.list.getSelectedValue());
				model.remove(this.list.getSelectedIndex());
				rv.update(rv.getActualDay());

			}
		});
		this.setVisible(true);
		this.pack();

	}

}