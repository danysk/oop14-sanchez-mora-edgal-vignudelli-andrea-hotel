/*
 * Questa GUI permette, per ogni stanza occupata, aggiungere/rimuovere servizi extra dalla prenotazione
 */
package vieww;

import java.awt.FlowLayout;
import java.util.Arrays;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;

import model.Booking;
import model.Extra;
import model.Hotel;

// TODO: Auto-generated Javadoc
/**
 * The Class ExtraBooking.
 */
public class ExtraBooking extends JFrame {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -895301204719429902L;
	
	/** The add button. */
	private final JButton add = new JButton("Aggiungi servizio extra");
	
	/** The remove button . */
	private final JButton remove = new JButton("Rimuovi tutti gli extra");
	
	/** The available extra list model. */
	private DefaultListModel<Extra> availableListModel = new DefaultListModel<>();
	
	/** The selected extra list model. */
	private DefaultListModel<Extra> selectedListModel = new DefaultListModel<>();
	
	/** The available extra list. */
	JList<Extra> availableList;
	
	/** The selected extra list. */
	JList<Extra> selectedList;

	/**
	 * Instantiates a new extra booking.
	 *
	 * @param b the booking where to add the extra services
	 */
	ExtraBooking(final Booking b) {
		this.setTitle("Aggiunta extra");
		this.setLayout(new FlowLayout());
		availableList = new JList<>(availableListModel);
		selectedList = new JList<>(selectedListModel);
		for (Extra e : Hotel.getInstance().getExtraList()) {
			availableListModel.addElement(e);
		}
		if (b.getExtraList() != null) {
			if (b.getExtraList().size() > 0) {
				for (Extra e : b.getExtraList()) {
					selectedListModel.addElement(e);
				}
			}
		}

		add.addActionListener(e -> {
			if (availableList.getSelectedIndex() >= 0) {
				if (!selectedListModel.contains(availableList.getSelectedValue())) {
					selectedListModel.addElement(availableList.getSelectedValue());
					if (selectedListModel != null) {
						if (!selectedListModel.isEmpty()) {
							for (Object extra : Arrays.asList(selectedListModel.toArray())) {
								b.addExtra((Extra) extra);
							}
						}
					}
				}
			}

		});
		remove.addActionListener(e -> {
			selectedListModel.removeAllElements();
			b.removeExtra();
		});
		this.add(new JScrollPane(availableList));
		this.add(add);
		this.add(remove);
		this.add(new JScrollPane(selectedList));
		this.setVisible(true);
		this.pack();

	}

}