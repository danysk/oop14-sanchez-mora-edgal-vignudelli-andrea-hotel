package controller.interfaces;

import java.util.Calendar;

public interface IDayController {
	
	void day(int a);
	void month(int a);
	String getDateString();
	Calendar getDate();
	
	
	
	
	

}
