package model;

import java.io.Serializable;

public enum SeasonType implements Serializable {
	LOWSEASON, MIDSEASON, HIGHSEASON;
}
